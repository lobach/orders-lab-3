﻿namespace OrdersLab3.Code;

public class U : IU
{
    private readonly TaskData _taskData;
    private readonly Dictionary<KW, int> _results;

    public U(TaskData taskData)
    {
        _taskData = taskData;
        _results = new Dictionary<KW, int>();
    }

    public int Evaluate(int k, int w)
    {
        var key = new KW(k, w);
        if (_results.ContainsKey(key))
            return _results[key];

        var result = k == 0
            ? _taskData.OrdersDifficult[0] <= w
                ? _taskData.OrdersPrice[0]
                : 0
            : _taskData.OrdersDifficult[k] <= w
                ? Math.Max(Evaluate(k - 1, w),
                    Evaluate(k - 1, w - _taskData.OrdersDifficult[k]) + _taskData.OrdersPrice[k])
                : Evaluate(k - 1, w);

        _results[key] = result;
        return result;
    }

    public override string ToString()
    {
        var str = "";
        var arr = new int[_taskData.Performance+1, _taskData.OrdersCount];
        foreach (var i in _results) 
            arr[i.Key.W, i.Key.K] = i.Value;

        for (var i = 0; i < _taskData.Performance + 1; i++)
        {
            str += $"{arr[i, 0]}";
            for (var j = 1; j < _taskData.OrdersCount; j++)
                str += $" {arr[i, j]}";

            str += '\n';
        }

        return str;
    }
}