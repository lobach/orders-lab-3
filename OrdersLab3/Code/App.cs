﻿using System.Diagnostics;

namespace OrdersLab3.Code;

public class App
{
    private readonly TaskData _taskData;

    public App(TaskData taskData) =>
        _taskData = taskData;

    public int Run(ISolver solver, int? max = null, int? prevResult = null)
    {
        var sw = Stopwatch.StartNew();
        var result = solver.Solve(max ?? _taskData.OrdersCount);
        sw.Stop();
        Console.WriteLine($"Result: {prevResult ?? result} in time: {sw.ElapsedMilliseconds} ms");
        return result;
    }
}