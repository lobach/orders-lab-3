﻿namespace OrdersLab3.Code;

public class TaskData
{
    public int Performance { get; init; }
    public int OrdersCount { get; init; }
    public int[] OrdersDifficult { get; init; }
    public int[] OrdersPrice { get; init; }

    public void Sort()
    {
        var data = new OrderData[OrdersCount];
        for (var i = 0; i < OrdersCount; i++)
            data[i] = new OrderData(OrdersDifficult[i], OrdersPrice[i]);

        Array.Sort(data, (a, b) => a.Efficiency < b.Efficiency
            ? 1
            : a.Efficiency > b.Efficiency
                ? -1
                : 0);

        for (var i = 0; i < OrdersCount; i++)
        {
            OrdersDifficult[i] = data[i].Difficult;
            OrdersPrice[i] = data[i].Price;
        }
    }

    public void SortCustom()
    {
        var data = new OrderData[OrdersCount];
        for (var i = 0; i < OrdersCount; i++)
            data[i] = new OrderData(OrdersDifficult[i], OrdersPrice[i]);

        Array.Sort(data, (a, b) => a.CustomEfficiency < b.CustomEfficiency
            ? 1
            : a.CustomEfficiency > b.CustomEfficiency
                ? -1
                : 0);

        for (var i = 0; i < OrdersCount; i++)
        {
            OrdersDifficult[i] = data[i].Difficult;
            OrdersPrice[i] = data[i].Price;
        }
    }
}

public struct OrderData
{
    public int Difficult;
    public int Price;

    public float Efficiency => (float) Price / Difficult;
    public float CustomEfficiency => Efficiency + (-0.5f / Difficult);

    public OrderData(int difficult, int price)
    {
        Difficult = difficult;
        Price = price;
    }
}