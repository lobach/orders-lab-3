﻿namespace OrdersLab3.Code;

public static class Extensions
{
    public static int ToInt(this string str) =>
        Convert.ToInt32(str);
}