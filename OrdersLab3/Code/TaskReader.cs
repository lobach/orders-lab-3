﻿namespace OrdersLab3.Code;

public static class TaskReader
{
    public static TaskData Read(string fileName)
    {
        var lines = File.ReadAllLines(fileName);
        var performance = lines[0].ToInt();
        var ordersCount = lines[1].ToInt();
        var ordersDifficult = lines[2]
            .Replace('\t', ' ')
            .Split(' ')
            .Where(s => !string.IsNullOrEmpty(s))
            .Select(s => s.ToInt())
            .ToArray();
        
        var ordersPrice = lines[3]
            .Replace('\t', ' ')
            .Split(' ')
            .Where(s => !string.IsNullOrEmpty(s))
            .Select(s => s.ToInt())
            .ToArray();

        return new TaskData
        {
            Performance = performance,
            OrdersCount = ordersCount,
            OrdersDifficult = ordersDifficult,
            OrdersPrice = ordersPrice
        };
    }
}