﻿namespace OrdersLab3.Code;

public interface ISolver
{
    public int Solve(int to);
}