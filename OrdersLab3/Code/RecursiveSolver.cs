﻿namespace OrdersLab3.Code;

public class RecursiveSolver : ISolver
{
    private readonly IU _u;
    private readonly TaskData _taskData;

    public RecursiveSolver(IU u, TaskData taskData)
    {
        _u = u;
        _taskData = taskData;
    }

    public int Solve(int to) => 
        _u.Evaluate(to - 1, _taskData.Performance);
}