﻿namespace OrdersLab3.Code;

public struct KW : IEquatable<KW>
{
    public int K;
    public int W;

    public KW(int k, int w)
    {
        K = k;
        W = w;
    }

    public bool Equals(KW other)
    {
        return K == other.K && W == other.W;
    }

    public override bool Equals(object? obj)
    {
        return obj is KW other && Equals(other);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(K, W);
    }
}