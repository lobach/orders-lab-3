﻿namespace OrdersLab3.Code;

public class TableSolver : ISolver
{
    private readonly TaskData _taskData;
    private Table _table;

    public TableSolver(TaskData taskData) => 
        _taskData = taskData;

    public int Solve(int to)
    {
        _table = new Table(_taskData);
        for (var i = 0; i < to; i++) 
            _table.Calc(i);

        return _table.Result;
    }

    public void PrintTable()
    {
        for (var i = 0; i < _taskData.Performance; i++) 
            Console.WriteLine($"{_table.Data[i, 0]} {_table.Data[i, 1]}");
    }
}