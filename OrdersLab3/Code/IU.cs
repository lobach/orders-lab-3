﻿namespace OrdersLab3.Code;

public interface IU
{
    public int Evaluate(int k, int w);
}