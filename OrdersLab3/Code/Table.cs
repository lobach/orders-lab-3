﻿namespace OrdersLab3.Code;

public class Table
{
    private const int BufferSize = 2;

    public int Result => Data[_taskData.Performance - 1, BufferSize - 1];
    public int[,] Data { get; }

    private readonly TaskData _taskData;

    public Table(TaskData taskData)
    {
        Data = new int[taskData.Performance, BufferSize];
        _taskData = taskData;
    }

    public void Calc(int k)
    {
        Swap();
        for (var i = 0; i < _taskData.Performance; i++)
        {
            if (k == 0)
                Data[i, 1] = Data[i, 0] = _taskData.OrdersDifficult[0] <= i
                    ? _taskData.OrdersPrice[0]
                    : 0;
            else
                Data[i, 1] = _taskData.OrdersDifficult[k] <= i
                    ? Math.Max(Data[i, 0], Data[i - _taskData.OrdersDifficult[k], 0] + _taskData.OrdersPrice[k])
                    : Data[i, 0];
        }
    }

    private void Swap()
    {
        for (var i = 0; i < _taskData.Performance; i++)
            Data[i, 0] = Data[i, 1];
    }

    private int Evaluate(int k, int w)
    {
        return k == 0
            ? _taskData.OrdersDifficult[0] <= w
                ? _taskData.OrdersPrice[0]
                : 0
            : _taskData.OrdersDifficult[k] <= w
                ? Math.Max(Evaluate(k - 1, w),
                    Evaluate(k - 1, w - _taskData.OrdersDifficult[k]) + _taskData.OrdersPrice[k])
                : Evaluate(k - 1, w);
    }
}