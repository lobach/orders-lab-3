﻿using OrdersLab3.Code;

namespace OrdersLab3;

public static class Program
{
    public static void Main(string[] args)
    {
        var path = "./Data";
        var dir = new DirectoryInfo(path!);
        var i = 0;
        Console.WriteLine("=======================");
        foreach (var file in dir.GetFiles())
        {
            Console.WriteLine($"FILE: {file.Name}");
            var taskData = TaskReader.Read(file.FullName);
            var app = new App(taskData);
            IU u = new U(taskData);
            ISolver solver = new RecursiveSolver(u, taskData);

            Console.WriteLine("RECURSIVE");
            var result = app.Run(solver);
            Console.WriteLine("TABLE");
            solver = new TableSolver(taskData);
            app.Run(solver, prevResult: result);
            Console.WriteLine("CROP TABLE");
            taskData.Sort();
            solver = new TableSolver(taskData);
            app.Run(solver, taskData.OrdersCount / 3 + 1);
            Console.WriteLine("CROP TABLE CUSTOM");
            taskData.SortCustom();
            solver = new TableSolver(taskData);
            app.Run(solver, taskData.OrdersCount / 3 + 1);
            Console.WriteLine("=======================");
            i++;
        }
    }
}